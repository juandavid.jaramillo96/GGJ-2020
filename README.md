# GGJ-2020
The Global Game Jam (GGJ) is the world's largest game jam event (game creation) taking place around the world at physical locations. Think of it as a hackathon focused on game development. It is the growth of an idea that in today’s heavily connected world, we could come together, be creative, share experiences and express ourselves in a multitude of ways using video games – it is very universal.

The weekend stirs a global creative buzz in games, while at the same time exploring the process of development, be it programming, iterative design, narrative exploration or artistic expression. It is all condensed into a 48 hour development cycle. The GGJ encourages people with all kinds of backgrounds to participate and contribute to this global spread of game development and creativity.

## Theme
Repair

## Installation
Go to the 'Releases' section of the project, and download the lastest version.

## Contributors
![](https://66.media.tumblr.com/8fe8e7eb772230a2803e6ce831ff30b6/tumblr_pzloddBRmk1y4r7kfo1_250.png)
![](https://66.media.tumblr.com/de3dc887810763edd96582ea630ea6c2/tumblr_pzloddBRmk1y4r7kfo4_250.png)
![](https://66.media.tumblr.com/809a2f37045762012133bb2779c13693/tumblr_pzloddBRmk1y4r7kfo5_250.png)
![](https://66.media.tumblr.com/e2e77e131d97a21662f11d186720eadd/48173714b1a1510b-06/s250x400/ef2a38d6facc7ef7d518d928fd4ef76e9df6a004.png)

* Andrés Rodríguez, *3D Artist*
* Esteban Jiménez, *Programmer*
* Juan David Jaramillo, *Programmer*
* Yumary Cubillos, *Designer*

*Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. Please make sure to update tests as appropriate.*

## License
[MIT](https://choosealicense.com/licenses/mit/)


