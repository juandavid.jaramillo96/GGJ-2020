﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Speedometer : MonoDebug
{
    #region Properties

    [Space]
    public Rigidbody target;

    // Getters & Setters
    public float velocity => target != null ? target.velocity.magnitude : 0f;

    #endregion

    #region Unity functions

    private void Update()
    {
        CheckSpeed();
    }

    #endregion

    #region Class functions

    public override void GetComponents()
    {
        if (target == null)
            target = GetComponent<Rigidbody>();
    }

    // ***

    private void CheckSpeed()
    {
        if (target != null)
            Log(string.Format("{0}, velocity: {1}", target.name, velocity));
    }

    #endregion
}
