///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 26/07/2019 10:38
///-----------------------------------------------------------------

using System;
using UnityEngine;

namespace Collections
{
    namespace UI
    {
        [Serializable]
        public class Screen
        {
            #region Properties

            public string name;
            public GameObject view;
            public Screen[] subscreens;

            #endregion
        }
    }
}
