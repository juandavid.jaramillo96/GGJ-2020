///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 15/02/2019 14:01
///   Company: NEDIAR
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Interlude : MonoDebug {

    #region Properties

    [Space, SerializeField]
    private bool fadeFromStart;

    [Space, SerializeField]
    private bool doFadeIn = true, doFadeOut = true;

    // ***

    [Space, SerializeField]
    private float delay;
    [SerializeField]
    private float timeToLoadNextScene, timeToWait, timeToFade;

    [SerializeField, Header("Images to fade")]
    private Image[] images;

    [SerializeField]
    private string FadeIgnoreTag = "FadeIgnore";

    // ***

    [Space, SerializeField]
    private LeanTweenType fadeInCurve;
    [SerializeField]
    private LeanTweenType fadeOutCurve;

    // ***

    [Space, SerializeField]
    private UnityEvent onInterludeEnd;

    // Const
    private readonly int BLACK_OUT = 0;
    private readonly int WHITE_IN = 1;

    private readonly Color BLANK = new Color(Color.white.r, Color.white.g, Color.white.b, 0);

    // Coroutines
    private IEnumerator fade;

    #endregion

    #region Unity functions

    private void Start()
    {
        if (fadeFromStart)
            StartFade();
    }

    #endregion

    #region Class functions

    public void LoadNextScene()
    {
        //SceneLoader.Instance.LoadNextScene();
    }

    // ***

    public void StartFade()
    {
        StopFade();

        Log("Interlude :: StartFade()");

        fade = Fade();

        StartCoroutine(fade);
    }

    public void StopFade()
    {
        if (fade != null)
        {
            Log("Interlude :: StopFade()");

            StopCoroutine(fade);
        }
    }

    private void FadeIn()
    {
        if (doFadeIn)
        {
            foreach (var image in images)
            {
                if (!image.CompareTag(FadeIgnoreTag))
                    LeanTween.alpha(image.rectTransform, WHITE_IN, timeToFade).setEase(fadeInCurve);
            }
        }
    }

    private void FadeOut()
    {
        if (doFadeOut)
        {
            foreach (var image in images)
            {
                if (!image.CompareTag(FadeIgnoreTag))
                    LeanTween.alpha(image.rectTransform, BLACK_OUT, timeToFade).setEase(fadeOutCurve);
            }
        }
    }

    #endregion

    #region Coroutines

    private IEnumerator Fade()
    {
        foreach (var image in images)
        {
            if (image.color.a != BLACK_OUT)
                image.color = new Color(image.color.r, image.color.g, image.color.b, BLANK.a);
        }

        yield return new WaitForSeconds(delay);

        if (images != null)
        {
            FadeIn();

            yield return new WaitForSeconds(timeToWait);

            FadeOut();
        }

        yield return new WaitForSeconds(timeToLoadNextScene);

        if (onInterludeEnd != null)
            onInterludeEnd.Invoke();
    }

    #endregion
}
