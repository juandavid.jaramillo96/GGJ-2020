///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/04/2019 17:34
///-----------------------------------------------------------------

public interface IProcess
{
    #region Class functions

    void Start();
    void End();

    #endregion
}
