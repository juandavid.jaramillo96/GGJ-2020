///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 30/04/2019 12:32
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Ticker : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private float initialTimeToTick;
    [SerializeField]
    private float timeToTick;

    [Space, SerializeField]
    private UnityEvent onTick;

    [Space, SerializeField]
    private bool executesFromStart;

    // Coroutines
    private IEnumerator tickCoroutine;

    #endregion

    #region Class functions

	public override void Setup()
	{
        if (executesFromStart)
            StartTick();
	}

    // ***

    public void StartTick()
    {
        StopTick();

        Log("Ticker :: StartTick()");

        tickCoroutine = Tick();

        StartCoroutine(tickCoroutine);
    }

    public void StopTick()
    {
        if (tickCoroutine != null)
        {
            Log("Ticker :: StopTick()");

            StopCoroutine(tickCoroutine);

            tickCoroutine = null;
        }
    }

    #endregion

    #region Coroutines

    private IEnumerator Tick()
    {
        if (initialTimeToTick != timeToTick)
        {
            if (isActiveAndEnabled)
            {
                yield return (initialTimeToTick != 0) ? new WaitForSeconds(initialTimeToTick) : null;

                onTick?.Invoke();
            }
        }

        while (isActiveAndEnabled)
        {
            yield return (timeToTick != 0) ? new WaitForSeconds(timeToTick) : null;

            onTick?.Invoke();
        }
    }

    #endregion
}
