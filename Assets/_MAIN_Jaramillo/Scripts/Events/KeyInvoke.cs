///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using UnityEngine;

public class KeyInvoke : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private KeyCode[] keys;
    [SerializeField]
    private Collections.Events.EventWrapper @event;

	#endregion

	#region Unity functions

	private void Update()
	{
        TryInvoke();
	}

	#endregion
	
	#region Class functions

    private void TryInvoke()
    {
        foreach (var key in keys)
        {
            if (Input.GetKeyDown(key))
            {
                Log(string.Format("KeyInvoke :: Invoke() :: {0}", key.ToString()));

                @event?.Invoke();
            }
        }
    }

	#endregion
}
