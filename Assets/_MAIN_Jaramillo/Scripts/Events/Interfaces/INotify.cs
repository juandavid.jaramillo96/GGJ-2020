///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

/// <summary>
/// Public interface that allows you trigger class callbacks
/// </summary>
public interface INotify<T>
{
    void Notify(T element);
}
