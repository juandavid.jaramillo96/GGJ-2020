///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;

namespace Collections
{
    namespace Events
    {
        public class EventHolder : MonoDebug
        {
            #region Properties

            [Space, SerializeField]
            private float delay;
            [SerializeField]
            private EventWrapper @event;

            [Space, Tooltip("Executes event from start."), SerializeField]
            private bool executeFromStart;

            #endregion

            #region Unity functions

            protected override void Start()
            {
                if (executeFromStart)
                    Invoke();
            }

            #endregion

            #region Class functions

            public void Invoke()
            {
                Log("Invokable :: Invoke()");

                StartCoroutine(InvokeCoroutine());
            }

            #endregion

            #region Coroutines

            private IEnumerator InvokeCoroutine()
            {
                if (delay > 0)
                    yield return new WaitForSeconds(delay);

                @event?.Invoke();

                yield return null;
            }

            #endregion
        }
    }
}
