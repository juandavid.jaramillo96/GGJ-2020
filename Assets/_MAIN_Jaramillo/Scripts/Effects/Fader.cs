///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 25/04/2019 17:57
///-----------------------------------------------------------------

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Fader : MonoDebug
{
    #region Properties

    [Space, SerializeField, Range(0,1), Tooltip("Intial fade value")]
    private float from;
    [SerializeField, Range(0, 1), Tooltip("Last fade value")]
    private float to;

    [Space, SerializeField]
    private float fadeTime;
    [SerializeField]
    private float fadeDelay;

    [Space, SerializeField]
    private LeanTweenType fadeCurve;

    [Space, SerializeField]
    private DynamicFloatEvent onFadeUpdate;
    [SerializeField]
    private UnityEvent onFadeStart, onFadeEnd;

    [Space, SerializeField]
    private bool logByName;

    [Space, SerializeField]
    private bool fadesFromStart;

    // Getters & Setters
    public float From
    {
        get
        {
            return from;
        }
    }

    public float To
    {
        get
        {
            return to;
        }
    }

    public float FadeTime
    {
        get
        {
            return fadeTime;
        }
    }

    public float FadeDelay
    {
        get
        {
            return fadeDelay;
        }
    }

    public LeanTweenType FadeCurve
    {
        get
        {
            return fadeCurve;
        }
    }

    // Hidden
    [HideInInspector]
    public Action<float> onFadeUpdateCallback;
    [HideInInspector]
    public Action onFadeStartCallback, onFadeEndCallback;

    // Coroutines
    private IEnumerator fade;

    #endregion

    #region Class functions

    public override void Setup()
	{
        if (fadesFromStart)
            StartFade();
	}

    // ***

    public void StartFade()
    {
        StopFade();

        TryLog("StartFade()");

        fade = Fade();

        StartCoroutine(fade);
    }

    public void StopFade()
    {
        if (fade != null)
        {
            TryLog("StopFade()");

            StopCoroutine(fade);

            fade = null;
        }
    }

    // ***

    private void OnFadeUpdate(float value)
    {
        TryLog(string.Format("OnFadeUpdate() :: {0}", value));

        onFadeUpdateCallback?.Invoke(value);

        onFadeUpdate?.Invoke(value);
    }

    private void OnFadeStart()
    {
        TryLog("OnFadeStart()");

        onFadeStartCallback?.Invoke();

        onFadeStart?.Invoke();
    }

    private void OnFadeEnd()
    {
        TryLog("OnFadeEnd()");

        onFadeEndCallback?.Invoke();

        onFadeEnd?.Invoke();
    }

    // ***

    private void TryLog(string callback)
    {
        if (logByName)
            Log(string.Format("{0} :: {1}", gameObject.name, callback));
        else
            Log(string.Format("Fader :: {0}", callback));
    }

    #endregion

    #region Coroutines

    private IEnumerator Fade()
    {
        yield return new WaitForSeconds(fadeDelay);

        OnFadeStart();

        LeanTween
            .value(gameObject: this.gameObject, from: this.from, to: this.to, time: fadeTime, callOnUpdate: OnFadeUpdate)
            .setEase(fadeCurve)
            .setOnComplete(OnFadeEnd);
    }

    #endregion
}
