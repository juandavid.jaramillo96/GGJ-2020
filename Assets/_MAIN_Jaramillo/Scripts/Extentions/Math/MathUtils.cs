///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 25/09/2018 13:25
///-----------------------------------------------------------------

using System;

public static class MathUtils
{
	#region Class functions

    public static int PowerOfTwo(int pow)
    {
        return (int)Math.Pow(2, pow);
    }

    public static int LogBaseTwo(int number)
    {
        return (int)Math.Log(number, 2);
    }

    public static float Remap(this float value, float fromStartedValue, float toStartedValue, float fromEndedValue, float toEndedValue)
    {
        return (value - fromStartedValue) / (toStartedValue - fromStartedValue) * (toEndedValue - fromEndedValue) + fromEndedValue;
    }

    public static bool IsInBetween(float value, float firstBound, float lastBound, bool includeFirstBound = true, bool includeLastBound = true)
    {
        if (includeFirstBound && includeLastBound)
            return (value >= firstBound && value <= lastBound);
        else if (includeFirstBound)
            return (value >= firstBound && value < lastBound);
        else if (includeLastBound)
            return (value > firstBound && value <= lastBound);
        else
            return (value > firstBound && value < lastBound);
    }

    #endregion
}
