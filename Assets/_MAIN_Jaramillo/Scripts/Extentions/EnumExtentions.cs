///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 11/02/2019 10:55
///-----------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;

public static class EnumExtentions
{
    #region Class functions

    /// <summary>
    /// Returns values from a specific enum type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IEnumerable<T> GetValues<T>() where T : Enum
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }

    /// <summary>
    /// Returns a random value from specific enum type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T GetRandomEnumValue<T>() where T: Enum
    {
        var values = GetValues<T>().ToArray();

        var random = new Random();

        T randomValue = (T)values.GetValue(random.Next(values.Length));

        return randomValue;
    }

    #endregion
}
