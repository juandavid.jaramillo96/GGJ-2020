﻿///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/04/2019 11:01
///-----------------------------------------------------------------

using System.Collections.Generic;

namespace RackFail
{
    public static class InputExtentions
    {
        #region Properties

        private static readonly Dictionary<string, ButtonIdentifier> buttonMap = new Dictionary<string, ButtonIdentifier>()
        {
            ["a"] = ButtonIdentifier.A,
            ["b"] = ButtonIdentifier.B,
            ["x"] = ButtonIdentifier.X,
            ["y"] = ButtonIdentifier.Y,

            ["dpadup"] = ButtonIdentifier.DPadUp,
            ["dpaddown"] = ButtonIdentifier.DPadDown,
            ["dpadleft"] = ButtonIdentifier.DPadLeft,
            ["dpadright"] = ButtonIdentifier.DPadRight,

            ["guide"] = ButtonIdentifier.Guide,
            ["back"] = ButtonIdentifier.Back,
            ["start"] = ButtonIdentifier.Start,

            ["l3"] = ButtonIdentifier.L3,
            ["r3"] = ButtonIdentifier.R3,

            ["lb"] = ButtonIdentifier.LB,
            ["rb"] = ButtonIdentifier.RB
        };

        private static readonly Dictionary<string, ThumbStickIdentifier> thumbStickMap = new Dictionary<string, ThumbStickIdentifier>()
        {
            ["leftthumbstick"] = ThumbStickIdentifier.LeftThumbStick,
            ["rightthumbstick"] = ThumbStickIdentifier.RightThumbStick
        };

        private static readonly Dictionary<string, TriggerIdentifier> triggerMap = new Dictionary<string, TriggerIdentifier>()
        {
            ["lefttrigger"] = TriggerIdentifier.LeftTrigger,
            ["righttrigger"] = TriggerIdentifier.RightTrigger
        };

        #endregion

        #region Class functions

        public static ButtonIdentifier ToButtonIdentifier(this string button)
        {
            if(!string.IsNullOrEmpty(button))
                return buttonMap.ContainsKey(button.ToLower()) ? buttonMap[button.ToLower()] : ButtonIdentifier.None;
            
            return ButtonIdentifier.None;
        }

        public static ThumbStickIdentifier ToThumbStickIdentifier(this string button)
        {
            if (!string.IsNullOrEmpty(button))
                return thumbStickMap.ContainsKey(button.ToLower()) ? thumbStickMap[button.ToLower()] : ThumbStickIdentifier.None;
            
            return ThumbStickIdentifier.None;
        }

        public static TriggerIdentifier ToTriggerIdentifier(this string button)
        {
            if (!string.IsNullOrEmpty(button))
                return triggerMap.ContainsKey(button.ToLower()) ? triggerMap[button.ToLower()] : TriggerIdentifier.None;
            
            return TriggerIdentifier.None;
        }

        #endregion
    }
}