﻿///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/04/2019 11:01
///-----------------------------------------------------------------

public enum ButtonIdentifier
{
    None,
    A,
    B,
    X,
    Y,
    DPadUp,
    DPadDown,
    DPadLeft,
    DPadRight,
    Guide,
    Back,
    Start,
    LB,
    RB,
    L3,
    R3
}

public enum ThumbStickIdentifier
{
    None,
    LeftThumbStick,
    RightThumbStick
}

public enum TriggerIdentifier
{
    None,
    LeftTrigger,
    RightTrigger
}