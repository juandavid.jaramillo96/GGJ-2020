﻿///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/04/2019 11:01
///-----------------------------------------------------------------

using System;
using System.Linq;
using UnityEngine;
using XInputDotNetPure;

namespace RackFail
{
    public class InputManager : MonoDebug
    {
        #region Properties

        [Space]
        public int index;

        [Space]
        public bool invertLeftThumbStickX;
        public bool invertLeftThumbStickY;
        public bool invertLeftThumbStickAxisCoordinates;

        [Space]
        public bool invertRightThumbStickX;
        public bool invertRightThumbStickY;
        public bool invertRightThumbStickAxisCoordinates;

        // Const
        private float TRIGGER_THRESHOLD = 0.1f;

        // Cached Components
        private ButtonIdentifier cachedButton;
        private ThumbStickIdentifier cachedThumbStick;
        private TriggerIdentifier cachedTrigger;

        // XInput .NET
        private bool setPlayerIndex = false;
        private PlayerIndex playerIndex;
        private GamePadState previousState;
        private GamePadState currentState;

        #endregion

        #region Unity functions

        private void Update()
        {
            CheckController();

            UpdateState();
        }

        #endregion

        #region Class functions

        /// <summary>
        /// Returns true when an axis has been pressed and not released.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButton(ButtonIdentifier button)
        {
            var value = GetButtonState(currentState, button) == ButtonState.Pressed;

            Log(string.Format("InputManager :: GetButton() :: {0} :: {1}", button, value));

            return value;
        }

        /// <summary>
        /// Returns true when an axis has been pressed and not released.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButton(string button)
        {
            cachedButton = button.ToButtonIdentifier();

            return GetButton(cachedButton);
        }

        /// <summary>
        /// Returns true when an axis has been pressed and not released.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButton(int button)
        {
            cachedButton = (ButtonIdentifier)(Math.Abs(button) % EnumExtentions.GetValues<ButtonIdentifier>().Count());

            return GetButton(cachedButton);
        }

        // ***

        /// <summary>
        /// Returns true during the frame the user pressed down the virtual trigger identified by buttonName.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButtonDown(ButtonIdentifier button)
        {
            var value = 
                GetButtonState(previousState, button) == ButtonState.Released && 
                GetButtonState(currentState, button) == ButtonState.Pressed;

            Log(string.Format("InputManager :: GetButtonDown() :: {0} :: {1}", button, value));

            return value;
        }

        /// <summary>
        /// Returns true during the frame the user pressed down the virtual trigger identified by buttonName.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButtonDown(string button)
        {
            cachedButton = button.ToButtonIdentifier();

            return GetButtonDown(cachedButton);
        }

        /// <summary>
        /// Returns true during the frame the user pressed down the virtual trigger identified by buttonName.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButtonDown(int button)
        {
            cachedButton = (ButtonIdentifier)(Math.Abs(button) % EnumExtentions.GetValues<ButtonIdentifier>().Count());

            return GetButtonDown(cachedButton);
        }

        // ***

        /// <summary>
        /// Returns true the first frame the user releases the virtual trigger identified by buttonName.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButtonUp(ButtonIdentifier button)
        {
            var value = 
                GetButtonState(currentState, button) == ButtonState.Released &&
                GetButtonState(previousState, button) == ButtonState.Pressed;

            Log(string.Format("InputManager :: GetButtonUp() :: {0} :: {1}", button, value));

            return value;
        }

        /// <summary>
        /// Returns true the first frame the user releases the virtual trigger identified by buttonName.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButtonUp(string button)
        {
            cachedButton = button.ToButtonIdentifier();

            return GetButtonUp(cachedButton);
        }

        /// <summary>
        /// Returns true the first frame the user releases the virtual trigger identified by buttonName.
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButtonUp(int button)
        {
            cachedButton = (ButtonIdentifier)(Math.Abs(button) % EnumExtentions.GetValues<ButtonIdentifier>().Count());

            return GetButtonUp(cachedButton);
        }

        // ***

        /// <summary>
        /// Returns the value of the virtual thumb stick identified by thumb stick name.
        /// </summary>
        /// <param name="thumbStick"></param>
        /// <returns></returns>
        public Vector2 GetThumbStick(ThumbStickIdentifier thumbStick)
        {
            var value = Vector2.zero;

            switch (thumbStick)
            {
                case ThumbStickIdentifier.LeftThumbStick:
                    value = new Vector2(
                        (invertLeftThumbStickAxisCoordinates) ? currentState.ThumbSticks.Left.Y : currentState.ThumbSticks.Left.X * (invertLeftThumbStickX ? -1 : 1),
                        (invertLeftThumbStickAxisCoordinates) ? currentState.ThumbSticks.Left.X : currentState.ThumbSticks.Left.Y * (invertLeftThumbStickY ? -1 : 1));
                    break;
                case ThumbStickIdentifier.RightThumbStick:
                    value = new Vector2(
                        (invertRightThumbStickAxisCoordinates) ? currentState.ThumbSticks.Right.Y : currentState.ThumbSticks.Right.X * (invertRightThumbStickX ? -1 : 1),
                        (invertRightThumbStickAxisCoordinates) ? currentState.ThumbSticks.Right.X : currentState.ThumbSticks.Right.Y * (invertRightThumbStickY ? -1 : 1));
                    break;
            }

            Log(string.Format("InputManager :: GetThumbStick() :: {0} :: {1}", thumbStick, value));
            
            return value;
        }

        /// <summary>
        /// Returns the value of the virtual thumb stick identified by thumb stick name.
        /// </summary>
        /// <param name="thumbStick"></param>
        /// <returns></returns>
        public Vector2 GetThumbStick(string thumbStick)
        {
            cachedThumbStick = thumbStick.ToThumbStickIdentifier();

            return GetThumbStick(cachedThumbStick);
        }

        /// <summary>
        /// Returns the value of the virtual thumb stick identified by thumb stick name.
        /// </summary>
        /// <param name="thumbStick"></param>
        /// <returns></returns>
        public Vector2 GetThumbStick(int thumbStick)
        {
            cachedThumbStick = (ThumbStickIdentifier)(Math.Abs(thumbStick) % EnumExtentions.GetValues<ThumbStickIdentifier>().Count());

            return GetThumbStick(cachedThumbStick);
        }

        // ***

        /// <summary>
        /// Returns the value of the virtual trigger identified by trigger name.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public float GetTrigger(TriggerIdentifier trigger)
        {
            var value = 0f;

            switch (trigger)
            {
                case TriggerIdentifier.LeftTrigger:
                    value = currentState.Triggers.Left;
                    break;
                case TriggerIdentifier.RightTrigger:
                    value = currentState.Triggers.Right;
                    break;
            }

            Log(string.Format("InputManager :: GetTrigger() :: {0} :: {1}", trigger, value));

            return value;
        }

        /// <summary>
        /// Returns the value of the virtual trigger identified by trigger name.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public float GetTrigger(string trigger)
        {
            cachedTrigger = trigger.ToTriggerIdentifier();

            return GetTrigger(cachedTrigger);
        }

        /// <summary>
        /// Returns the value of the virtual trigger identified by trigger name.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public float GetTrigger(int trigger)
        {
            cachedTrigger = (TriggerIdentifier)(Math.Abs(trigger) % EnumExtentions.GetValues<TriggerIdentifier>().Count());

            return GetTrigger(cachedTrigger);
        }

        // ***

        /// <summary>
        /// Returns true when a virtual trigger identified by trigger name has been pressed and not released.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public bool GetTriggerPulse(TriggerIdentifier trigger)
        {
            var value = false;

            switch (trigger)
            {
                case TriggerIdentifier.LeftTrigger:
                    value = previousState.Triggers.Left <= TRIGGER_THRESHOLD && currentState.Triggers.Left >= TRIGGER_THRESHOLD;
                    break;
                case TriggerIdentifier.RightTrigger:
                    value = previousState.Triggers.Right <= TRIGGER_THRESHOLD && currentState.Triggers.Right >= TRIGGER_THRESHOLD;
                    break;
            }

            Log(string.Format("InputManager :: GetTriggerPulse() :: {0} :: {1}", trigger, value));

            return value;
        }

        /// <summary>
        /// Returns true when a virtual trigger identified by trigger name has been pressed and not released.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public bool GetTriggerPulse(string trigger)
        {
            cachedTrigger = trigger.ToTriggerIdentifier();

            return GetTriggerPulse(cachedTrigger);
        }

        /// <summary>
        /// Returns true when a virtual trigger identified by trigger name has been pressed and not released.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public bool GetTriggerPulse(int trigger)
        {
            cachedTrigger = (TriggerIdentifier)(Math.Abs(trigger) % EnumExtentions.GetValues<TriggerIdentifier>().Count());

            return GetTriggerPulse(cachedTrigger);
        }

        // ***

        private ButtonState GetButtonState(GamePadState gamePadState, ButtonIdentifier button)
        {
            switch (button)
            {
                case ButtonIdentifier.A:
                    return gamePadState.Buttons.A;
                case ButtonIdentifier.B:
                    return gamePadState.Buttons.B;
                case ButtonIdentifier.X:
                    return gamePadState.Buttons.X;
                case ButtonIdentifier.Y:
                    return gamePadState.Buttons.Y;
                case ButtonIdentifier.DPadUp:
                    return gamePadState.DPad.Up;
                case ButtonIdentifier.DPadDown:
                    return gamePadState.DPad.Down;
                case ButtonIdentifier.DPadLeft:
                    return gamePadState.DPad.Left;
                case ButtonIdentifier.DPadRight:
                    return gamePadState.DPad.Right;
                case ButtonIdentifier.Guide:
                    return gamePadState.Buttons.Guide;
                case ButtonIdentifier.Back:
                    return gamePadState.Buttons.Back;
                case ButtonIdentifier.Start:
                    return gamePadState.Buttons.Start;
                case ButtonIdentifier.LB:
                    return gamePadState.Buttons.LeftShoulder;
                case ButtonIdentifier.RB:
                    return gamePadState.Buttons.RightShoulder;
                case ButtonIdentifier.L3:
                    return gamePadState.Buttons.LeftStick;
                case ButtonIdentifier.R3:
                    return gamePadState.Buttons.RightStick;
                default:
                    return ButtonState.Released;
            }
        }

        // ***

        private void CheckController()
        {
            if (!setPlayerIndex && !previousState.IsConnected)
            {
                PlayerIndex cachedPlayerIndex = (PlayerIndex)index;

                GamePadState testState = GamePad.GetState(cachedPlayerIndex);

                if (testState.IsConnected)
                {
                    Log(string.Format("Input :: GamePad {0} found", cachedPlayerIndex.ToString().ToLower()));

                    playerIndex = cachedPlayerIndex;

                    setPlayerIndex = true;
                }
            }
        }

        private void UpdateState()
        {
            previousState = currentState;
            currentState = GamePad.GetState(playerIndex);
        }

        #endregion
    }
}
