///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 10/07/2019 11:33
///   Company: NEDIAR
///-----------------------------------------------------------------

public enum UpdateMode
{
    Update,
    LateUpdate,
    PreRender,
    PreCull
}
