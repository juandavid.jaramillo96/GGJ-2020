using UnityEngine;

public class SphericalCoordinateTracking : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private Transform target;

    // Getters & Setters
    public Vector3 currentLocalPosition
    {
        get; private set;
    }

    public Vector3 lastLocalPosition
    {
        get; private set;
    }

    #endregion

    #region Unity functions

    private void Update()
    {
        TryPrintSphericalCoordinates();
    }

    #endregion

    #region Class functions

    private void TryPrintSphericalCoordinates()
    {
        if (target != null)
        {
            if (target.localPosition != currentLocalPosition)
            {
                // Setting last & current local position...
                if (lastLocalPosition != currentLocalPosition)
                    lastLocalPosition = currentLocalPosition;

                currentLocalPosition = target.localPosition;

                Log(SphericalCoordinate.FromCartesian(currentLocalPosition).ToString());
            }
        }
    }

    #endregion
}