    ///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 13/06/2019 13:01
///-----------------------------------------------------------------

using System;
using UnityEngine;
using UnityEngine.Events;

public class CollisionListener : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private DynamicCollisionEvent onCollisionEnter;
    [SerializeField]
    private DynamicCollisionEvent onCollisionStay, onCollisionExit;

    // Events
    [HideInInspector]
    public Action<Collision> onCollisionEnterAction, onCollisionStayAction, onCollisionExitAction; // TODO Use 'Event' instead of 'UnityEvent' and 'Action'

    #endregion

    #region Unity functions

    private void OnCollisionEnter(Collision collision)
    {
        Log(string.Format("CollisionListener :: OnCollisionEnter() :: {0}", collision.collider.name));

        onCollisionEnter?.Invoke(collision);
        onCollisionEnterAction.Invoke(collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        Log(string.Format("CollisionListener :: OnCollisionStay() :: {0}", collision.collider.name));

        onCollisionStay?.Invoke(collision);
        onCollisionStayAction?.Invoke(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        Log(string.Format("CollisionListener :: OnCollisionExit() :: {0}", collision.collider.name));

        onCollisionExit?.Invoke(collision);
        onCollisionExitAction?.Invoke(collision);
    }

    #endregion
}
