using System;

namespace UnityEngine
{
    namespace Events
    {
        [Serializable]
        public class DynamicBoolEvent : UnityEvent<bool>
        {
            // ...
        }

        [Serializable]
        public class DynamicStringEvent : UnityEvent<string>
        {
            // ...
        }

        [Serializable]
        public class DynamicIntegerEvent : UnityEvent<int>
        {
            // ...
        }

        [Serializable]
        public class DynamicFloatEvent : UnityEvent<float>
        {
            // ...
        }

        [Serializable]
        public class DynamicTransformEvent : UnityEvent<Transform>
        {
            // ...
        }

        [Serializable]
        public class DynamicGameObjectEvent : UnityEvent<GameObject>
        {
            // ...
        }

        [Serializable]
        public class DynamicRigidbodyEvent : UnityEvent<Rigidbody>
        {
            // ...
        }

        [Serializable]
        public class DynamicColliderEvent : UnityEvent<Collider>
        {
            // ...
        }

        [Serializable]
        public class DynamicCollisionEvent : UnityEvent<Collision>
        {
            // ...
        }
    }
}