///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 13/06/2019 13:01
///-----------------------------------------------------------------

// Taken from: http://blog.nobel-joergensen.com/2010/10/22/spherical-coordinates-in-unity/
// Taken from: http://en.wikipedia.org/wiki/Spherical_coordinate_system

using UnityEngine;

public class SphericalCoordinate
{
    #region Prooperties

    // Determine what happen when a limit is reached, repeat or clamp.
    public bool loopPolar = true, loopElevation = false;

    // Getters & Setters
    public float radius
    {
        get
        {
            return _radius;
        }
        private set
        {
            _radius = Mathf.Clamp(value, _minRadius, _maxRadius);
        }
    }

    public float polar
    {
        get
        {
            return _polar;
        }
        private set
        {
            _polar = loopPolar ? Mathf.Repeat(value, _maxPolar - _minPolar)
                               : Mathf.Clamp(value, _minPolar, _maxPolar);
        }
    }

    public float elevation
    {
        get
        {
            return _elevation;
        }
        private set
        {
            _elevation = loopElevation ? Mathf.Repeat(value, _maxElevation - _minElevation)
                                       : Mathf.Clamp(value, _minElevation, _maxElevation);
        }
    }

    public float minRadius
    {
        get
        {
            return _minRadius;
        }
    }

    public float maxRadius
    {
        get
        {
            return _maxRadius;
        }
    }

    public float minPolar
    {
        get
        {
            return _minPolar;
        }
    }

    public float maxPolar
    {
        get
        {
            return _maxPolar;
        }
    }

    public float minElevation
    {
        get
        {
            return _minElevation;
        }
    }

    public float maxElevation
    {
        get
        {
            return _maxElevation;
        }
    }

    // Cached properties
    private float _radius, _polar, _elevation;

    private float _minRadius, _maxRadius;
    private float _minPolar, _maxPolar;
    private float _minElevation, _maxElevation;

    #endregion

    #region Class functions

    // Constructors...

    public SphericalCoordinate()
    {
        // ...
    }

    public SphericalCoordinate(float radius, float polar, float elevation,
        float minRadius = 1f, float maxRadius = 20f,
        float minPolar = 0f, float maxPolar = (Mathf.PI * 2f),
        float minElevation = 0f, float maxElevation = (Mathf.PI / 3f))
    {
        _minRadius = minRadius;
        _maxRadius = maxRadius;
        _minPolar = minPolar;
        _maxPolar = maxPolar;
        _minElevation = minElevation;
        _maxElevation = maxElevation;

        SetRadius(radius);
        SetRotation(polar, elevation);
    }

    public SphericalCoordinate(Transform transform,
        float minRadius = 1f, float maxRadius = 20f,
        float minPolar = 0f, float maxPolar = (Mathf.PI * 2f),
        float minElevation = 0f, float maxElevation = (Mathf.PI / 3f)) :
        this(transform.position, minRadius, maxRadius, minPolar, maxPolar, minElevation, maxElevation)
    {
        // ...
    }

    public SphericalCoordinate(Vector3 cartesianCoordinate,
        float minRadius = 1f, float maxRadius = 20f,
        float minPolar = 0f, float maxPolar = (Mathf.PI * 2f),
        float minElevation = 0f, float maxElevation = (Mathf.PI / 3f))
    {
        _minRadius = minRadius;
        _maxRadius = maxRadius;
        _minPolar = minPolar;
        _maxPolar = maxPolar;
        _minElevation = minElevation;
        _maxElevation = maxElevation;

        FromCartesian(cartesianCoordinate);
    }

    // Static members...

    public static SphericalCoordinate FromCartesian(Vector3 cartesianCoordinate)
    {
        if (cartesianCoordinate.x == 0f)
            cartesianCoordinate.x = Mathf.Epsilon;

        var cachedRadius = cartesianCoordinate.magnitude;

        var cachedPolar = Mathf.Atan(cartesianCoordinate.z / cartesianCoordinate.x);

        if (cartesianCoordinate.x < 0f)
            cachedPolar += Mathf.PI;

        var cachedElevation = Mathf.Asin(cartesianCoordinate.y / cachedRadius);

        return new SphericalCoordinate(cachedRadius, cachedPolar, cachedElevation);
    }

    // Public members...

    public SphericalCoordinate RotatePolarAngle(float x)
    {
        return Rotate(x, 0f);
    }

    public SphericalCoordinate RotateElevationAngle(float x)
    {
        return Rotate(0f, x);
    }

    public SphericalCoordinate Rotate(float newPolar, float newElevation)
    {
        return SetRotation(polar + newPolar, elevation + newElevation);
    }

    public SphericalCoordinate SetPolarAngle(float x)
    {
        return SetRotation(x, elevation);
    }

    public SphericalCoordinate SetElevationAngle(float x)
    {
        return SetRotation(polar, x);
    }

    public SphericalCoordinate SetRotation(float newPolar, float newElevation)
    {
        polar = newPolar;

        elevation = newElevation;

        return this;
    }

    public SphericalCoordinate TranslateRadius(float x)
    {
        return SetRadius(radius + x);
    }

    public SphericalCoordinate SetRadius(float rad)
    {
        radius = rad;

        return this;
    }

    // Utils...

    public Vector3 ToCartesian()
    {
        float a = radius * Mathf.Cos(elevation);

        return new Vector3(a * Mathf.Cos(polar), radius * Mathf.Sin(elevation), a * Mathf.Sin(polar));
    }

    // ...
    
    public override string ToString()
    {
        return string.Format("Radius: {0}, Polar: {1}, Elevation: {2}", radius, polar, elevation);
    }

    #endregion
}
