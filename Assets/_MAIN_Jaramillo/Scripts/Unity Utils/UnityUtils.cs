///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///   Company: NEDIAR
///-----------------------------------------------------------------

namespace UnityEngine
{
    namespace Utils
    {
        public static class UnityUtils
        {
            #region Class functions

            /// <summary>
            /// Returns true if a GameObject has a specific Component.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="component"></param>
            /// <returns></returns>
            public static bool HasComponent<T>(this Component component) where T : Component
            {
                return (component != null) ? component.GetComponent<T>() != null : false;
            }

            /// <summary>
            /// Returns true if a GameObject has a specific Component.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="component"></param>
            /// <returns></returns>
            public static bool HasComponent<T>(this GameObject gameObject) where T : Component
            {
                return (gameObject != null) ? gameObject.GetComponent<T>() != null : false;
            }

            /// <summary>
            /// Activates/Desactivates a GameObject if is active self or not
            /// </summary>
            /// <param name="gameObject"></param>
            /// <param name="state"></param>
            public static void TrySetActive(this GameObject gameObject, bool state)
            {
                if (gameObject != null)
                {
                    if (gameObject.activeSelf != state)
                        gameObject.SetActive(state);
                }
            }

            /// <summary>
            /// Set Transform global position
            /// </summary>
            /// <param name="transform"></param>
            /// <param name="position"></param>
            public static void SetPosition(this Transform transform, Vector3 position)
            {
                if (transform != null)
                    transform.position = position;
            }

            /// <summary>
            /// Set Transform global rotattion in euler angles
            /// </summary>
            /// <param name="transform"></param>
            /// <param name="rotation"></param>
            public static void SetRotation(this Transform transform, Vector3 rotation)
            {
                if (transform != null)
                    transform.rotation = Quaternion.Euler(rotation);
            }

            /// <summary>
            /// Set Transform local position
            /// </summary>
            /// <param name="transform"></param>
            /// <param name="localPosition"></param>
            public static void SetLocalPosition(this Transform transform, Vector3 localPosition)
            {
                if (transform != null)
                    transform.localPosition = localPosition;
            }

            /// <summary>
            /// Set Transform local position in euler angles
            /// </summary>
            /// <param name="transform"></param>
            /// <param name="localRotation"></param>
            public static void SetLocalRotation(this Transform transform, Vector3 localRotation)
            {
                if (transform != null)
                    transform.localRotation = Quaternion.Euler(localRotation);
            }

            /// <summary>
            /// Set Transform local scale
            /// </summary>
            /// <param name="transform"></param>
            /// <param name="localScale"></param>
            public static void SetLocalScale(this Transform transform, Vector3 localScale)
            {
                if (transform != null)
                    transform.localScale = localScale;
            }

            /// <summary>
            /// Returns the Component that you whant to find. If there's not a Component in the GameObject, it add's itself.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="component"></param>
            /// <returns></returns>
            public static T GetOrAddComponent<T>(this Component component) where T : Component
            {
                if (!component.HasComponent<T>())
                    component.gameObject.AddComponent<T>();

                return component.GetComponent<T>();
            }

            /// <summary>
            /// Returns the Component that you whant to find. If there's not a Component in the GameObject, it add's itself.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="component"></param>
            /// <returns></returns>
            public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
            {
                if (!gameObject.HasComponent<T>())
                    gameObject.AddComponent<T>();

                return gameObject.GetComponent<T>();
            }

            #endregion
        }
    }
}
