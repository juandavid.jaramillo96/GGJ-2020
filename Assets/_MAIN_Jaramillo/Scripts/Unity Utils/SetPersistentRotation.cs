///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 25/06/2019 10:45
///-----------------------------------------------------------------

using UnityEngine;

public class SetPersistentRotation : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private Transform target;

    [Space, SerializeField]
    private bool xPersistent;
    [SerializeField]
    private bool yPersistent, zPersistent;

    [Space, SerializeField]
    private float xPersistentValue;
    [SerializeField]
    private float yPersistentValue, zPersistentValue;

    [Space, SerializeField]
    private bool xPersistentValueIsTargetInitialLocalRotation;
    [SerializeField]
    private bool yPersistentValueIsTargetInitialLocalRotation, zPersistentValueIsTargetInitialLocalRotation;

    [Space, SerializeField]
    private UpdateMode persistentUpdateMode;

    #endregion

    #region Unity functions

    private void OnEnable()
    {
        if (persistentUpdateMode == UpdateMode.PreRender)
            Camera.onPreRender += UpdateTargetRotation;

        if (persistentUpdateMode == UpdateMode.PreCull)
            Camera.onPreCull += UpdateTargetRotation;
    }

    private void OnDisable()
    {
        Camera.onPreRender -= UpdateTargetRotation;
        Camera.onPreCull -= UpdateTargetRotation;
    }

    protected void Update()
    {
        if (persistentUpdateMode == UpdateMode.Update)
            UpdateTargetRotation();
    }

    protected void LateUpdate()
    {
        if (persistentUpdateMode == UpdateMode.LateUpdate)
            UpdateTargetRotation();
    }

    #endregion

    #region Class functions

    public override void GetComponents()
	{
        if (target == null)
            target = transform;
	}

	public override void Setup()
	{
        // Sets intial 'x' value to target local rotation...
        xPersistentValue = (xPersistentValueIsTargetInitialLocalRotation) ? target.transform.localRotation.eulerAngles.x : xPersistentValue;
        // Sets intial 'y' value to target local rotation...
        yPersistentValue = (yPersistentValueIsTargetInitialLocalRotation) ? target.transform.localRotation.eulerAngles.y : yPersistentValue;
        // Sets intial 'z' value to target local rotation...
        zPersistentValue = (zPersistentValueIsTargetInitialLocalRotation) ? target.transform.localRotation.eulerAngles.z : zPersistentValue;
    }

    // ***

    private void UpdateTargetRotation(Camera camera)
    {
        UpdateTargetRotation();
    }

    private void UpdateTargetRotation()
    {
        Log("SetPersistentRotation :: UpdateTargetRotation()");

        // Updates target rotation base on persistent values...
        target.transform.rotation = Quaternion.Euler(
            new Vector3 (
                (xPersistent) ? xPersistentValue : target.rotation.eulerAngles.x,
                (yPersistent) ? yPersistentValue : target.rotation.eulerAngles.y,
                (zPersistent) ? zPersistentValue : target.rotation.eulerAngles.z
            )
        );
    }

    #endregion
}
