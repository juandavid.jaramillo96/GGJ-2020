///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 25/06/2019 10:45
///-----------------------------------------------------------------

using UnityEngine;

public class SetPersistentPosition : MonoDebug
{
    #region Properties

    [Space, SerializeField]
    private Transform target;

    [Space, SerializeField]
    private bool xPersistent;
    [SerializeField]
    private bool yPersistent, zPersistent;

    [Space, SerializeField]
    private float xPersistentValue;
    [SerializeField]
    private float yPersistentValue, zPersistentValue;

    [Space, SerializeField]
    private bool xPersistentValueIsTargetInitialLocalPosition;
    [SerializeField]
    private bool yPersistentValueIsTargetInitialLocalPosition, zPersistentValueIsTargetInitialLocalPosition;

    [Space, SerializeField]
    private UpdateMode persistentUpdateMode;

    #endregion

    #region Unity functions

    private void OnEnable()
    {
        if (persistentUpdateMode == UpdateMode.PreRender)
            Camera.onPreRender += UpdateTargetPosition;

        if (persistentUpdateMode == UpdateMode.PreCull)
            Camera.onPreCull += UpdateTargetPosition;
    }

    private void OnDisable()
    {
        Camera.onPreRender -= UpdateTargetPosition;
        Camera.onPreCull -= UpdateTargetPosition;
    }

    protected void Update()
    {
        if (persistentUpdateMode == UpdateMode.Update)
            UpdateTargetPosition();
    }

    protected void LateUpdate()
    {
        if (persistentUpdateMode == UpdateMode.LateUpdate)
            UpdateTargetPosition();
    }

    #endregion

    #region Class functions

    public override void GetComponents()
	{
        if (target == null)
            target = transform;
	}

	public override void Setup()
	{
        // Sets intial 'x' value to target local position...
        xPersistentValue = (xPersistentValueIsTargetInitialLocalPosition) ? target.transform.localPosition.x : xPersistentValue;
        // Sets intial 'y' value to target local position...
        yPersistentValue = (yPersistentValueIsTargetInitialLocalPosition) ? target.transform.localPosition.y : yPersistentValue;
        // Sets intial 'z' value to target local position...
        zPersistentValue = (zPersistentValueIsTargetInitialLocalPosition) ? target.transform.localPosition.z : zPersistentValue;
    }

    // ***

    private void UpdateTargetPosition(Camera camera)
    {
        UpdateTargetPosition();
    }

    private void UpdateTargetPosition()
    {
        Log("SetPersistentPosition :: UpdateTargetPosition()");

        // Updates target position base on persistent values...
        target.transform.position = new Vector3 (
            (xPersistent) ? xPersistentValue : target.position.x,
            (yPersistent) ? yPersistentValue : target.position.y,
            (zPersistent) ? zPersistentValue : target.position.z
        );
    }

    #endregion
}
