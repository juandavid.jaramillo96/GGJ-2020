///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 24/04/2019 09:19
///-----------------------------------------------------------------

public enum ToStringFormat
{
    OneSignificantFigures,
    TwoSignificantFigures,
    ThreeSignificantFigures,
    FourSignificantFigures,
    FiveSignificantFigures,
}
