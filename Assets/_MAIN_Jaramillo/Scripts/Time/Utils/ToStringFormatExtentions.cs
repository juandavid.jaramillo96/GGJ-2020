///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 24/04/2019 10:22
///-----------------------------------------------------------------

public static class ToStringFormatExtentions
{
    #region Class functions

    public static string ToString(this ToStringFormat format, int integer)
    {
        switch (format)
        {
            case ToStringFormat.OneSignificantFigures:
                return (integer % 10).ToString("0");

            case ToStringFormat.TwoSignificantFigures:
                return (integer % 100).ToString("00");

            case ToStringFormat.ThreeSignificantFigures:
                return (integer % 1000).ToString("000");

            case ToStringFormat.FourSignificantFigures:
                return (integer % 10000).ToString("0000");

            case ToStringFormat.FiveSignificantFigures:
                return (integer % 100000).ToString("00000");

            default:
                return (integer % 10).ToString();
        }
    }

    public static string ToString(this ToStringFormat format, float @decimal)
    {
        switch (format)
        {
            case ToStringFormat.OneSignificantFigures:
                return (@decimal % 10).ToString("0");

            case ToStringFormat.TwoSignificantFigures:
                return (@decimal % 100).ToString("00");

            case ToStringFormat.ThreeSignificantFigures:
                return (@decimal % 1000).ToString("000");

            case ToStringFormat.FourSignificantFigures:
                return (@decimal % 10000).ToString("0000");

            case ToStringFormat.FiveSignificantFigures:
                return (@decimal % 100000).ToString("00000");

            default:
                return @decimal.ToString();
        }
    }

    public static string ToString(this ToStringFormat format, double @decimal)
    {
        switch (format)
        {
            case ToStringFormat.OneSignificantFigures:
                return (@decimal % 10).ToString("0");

            case ToStringFormat.TwoSignificantFigures:
                return (@decimal % 100).ToString("00");

            case ToStringFormat.ThreeSignificantFigures:
                return (@decimal % 1000).ToString("000");

            case ToStringFormat.FourSignificantFigures:
                return (@decimal % 10000).ToString("0000");

            case ToStringFormat.FiveSignificantFigures:
                return (@decimal % 100000).ToString("00000");

            default:
                return @decimal.ToString();
        }
    }

    #endregion
}
