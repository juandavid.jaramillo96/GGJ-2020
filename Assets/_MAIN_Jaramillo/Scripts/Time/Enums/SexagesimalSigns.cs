///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 24/04/2019 13:42
///   Company: NEDIAR
///-----------------------------------------------------------------

public enum SexagesimalSigns
{
    Minutes = '\'',
    Seconds = '"',
}