///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 24/04/2019 07:58
///-----------------------------------------------------------------

using System;

[Flags]
public enum TimeUnits
{
    Days = 1 << 1,
    Hours = 1 << 2,
    Minutes = 1 << 3,
    Seconds = 1 << 4,
    Miliseconds = 1 << 5,
    All = Days | Hours | Minutes | Seconds | Miliseconds,
}
