///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 26/02/2019 09:02
///-----------------------------------------------------------------

using UnityEngine;

public class TimeScaler : MonoDebug
{
    #region Properties

    [Space, SerializeField, Range(0, 60)]
    private float timeScale = 1;

    // Getters & Setters
    public float TimeScale
    {
        get
        {
            return timeScale;
        }
    }

    // Hidden
    private float lastTimeScale;

	#endregion

	#region Unity functions

    private void Update()
    {
        SetTimeScale();
    }

	#endregion
	
	#region Class functions

    private void SetTimeScale()
    {
        if (timeScale != lastTimeScale)
        {
            Log(string.Format("TimeScaler :: SetTimeScale() :: {0}", timeScale));

            Time.timeScale = timeScale;

            lastTimeScale = timeScale;
        }
    }

	#endregion
}
