///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/11/2018 14:17
///-----------------------------------------------------------------

using System;
using UnityEngine;

/// <summary>
/// The 'Memento' class
/// </summary>
[Serializable]
public class Memento
{
    #region Properties

    [SerializeField]
    private object properties;

    // Getters & Setters
    public object State
    {
        get
        {
            return properties;
        }
    }

    #endregion

    #region Class functions

    // Constructor!
    public Memento(object properties)
    {
        this.properties = properties;
    }

    #endregion
}
