///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

/// <summary>
/// Public interface that allows you reset an object before it is released to a pool
/// </summary>
public interface IPooled
{
    void ResetObject();
}
