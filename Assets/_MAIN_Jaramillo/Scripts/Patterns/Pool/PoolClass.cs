///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///-----------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolClass : MonoDebug
{
    #region Properties

    [SerializeField]
    private int objectPoolSize;
    [SerializeField]
    private GameObject objectPrefab;

    /// <summary>
    /// Property that handle the entry and exit of objects to the pool
    /// </summary>
    [HideInInspector]
    public List<GameObject> objects = new List<GameObject>();

    // Cached components
    private new Transform transform;

    #endregion

    #region Unity functions

    protected override void Awake()
    {
        base.Awake();

        Prepare();
    }

    #endregion

    #region Class functions

    private void Prepare()
    {
        transform = GetComponent<Transform>();

        for (int i = 0; i < objectPoolSize; i++)
            AddObject();
    }

    // ***

    /// <summary>
    /// Function that allows you obtain an object from the pool
    /// </summary>
    /// <param name="position">Used to indicate the world release position</param>
    /// <returns></returns>
    public GameObject GetObject(Vector3 position)
    {
        if (objects.Count == 0)
            AddObject();

        return ObtainObject(position);
    }

    /// <summary>
    /// Function that allows you bring back an object to the pool
    /// </summary>
    /// <param name="object">Used to pass the released object</param>
    public void ReleaseObject(GameObject @object)
    {
        IPooled pooledObject = @object.GetComponent<IPooled>();
        
        if (pooledObject != null)
            pooledObject.ResetObject();

        @object.transform.parent = transform;
        @object.transform.position = Vector3.zero;
        @object.SetActive(false);
        objects.Add(@object);
    }

    // ***

    private void AddObject()
    {
        GameObject instance = Instantiate(objectPrefab, Vector3.zero, Quaternion.identity, transform);

        instance.SetActive(false);
        objects.Add(instance);
    }

    // ***

    private GameObject ObtainObject(Vector3 position)
    {
        GameObject @object = objects[objects.Count - 1];

        objects.RemoveAt(objects.Count - 1);
        @object.transform.parent = null;
        @object.SetActive(true);
        @object.transform.position = position;

        return @object;
    }

    #endregion

    #region Coroutines

    private IEnumerator Instantiate(GameObject gameObject)
    {
        yield return Object.Instantiate(gameObject);
    }

    #endregion
}
