﻿///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 16/04/2019 11:01
///-----------------------------------------------------------------

using System;
using UnityEngine;

namespace RackFail
{
    [RequireComponent(typeof(InputManager)), RequireComponent(typeof(Rigidbody))]
    public class CharacterController : MonoDebug
    {
        #region Properties

        [Header("Settings"), SerializeField] 
        private float _speed;
        [SerializeField]
        private float _maxSpeed;
        [SerializeField, Range(0, 1)]
        private float threshold;

        [Header("Components"), SerializeField] private Animator animator;
    
        // Getters & Setters
        public float speed => _speed;
        public float maxSpeed => _maxSpeed;

        // Cached Components
        private Vector2 leftThumbStick;
        private Vector2 rightThumbStick;

        private InputManager inputManager;

        private new Rigidbody rigidbody;

        #endregion

        #region Unity functions

        private void Update()
        {
            leftThumbStick = inputManager.GetThumbStick(ThumbStickIdentifier.LeftThumbStick);
            rightThumbStick = inputManager.GetThumbStick(ThumbStickIdentifier.RightThumbStick);
        }

        private void FixedUpdate()
        {
            if (Math.Abs(leftThumbStick.x) > threshold ||
                Math.Abs(leftThumbStick.y) > threshold)
            {
                Rotate();
                Move();
            }
            else
            {
                rigidbody.angularVelocity = Vector3.zero;

                animator?.SetBool("isMoving", false);
            }

            if (rigidbody.velocity.magnitude > _maxSpeed)
                rigidbody.velocity = rigidbody.velocity.normalized * _maxSpeed;
        }

        #endregion

        #region Class functions

        public override void GetComponents()
        {
            if (animator != null)
                animator = GetComponent<Animator>();

            inputManager = GetComponent<InputManager>();

            rigidbody = GetComponent<Rigidbody>();
        }

        // ***

        public void SetSpeed(float speed)
        {
            _speed = speed;
        }

        public void SetMaxSpeed(float maxSpeed)
        {
            _maxSpeed = maxSpeed;
        }

        // ***

        private void Move()
        {
            animator.SetBool("isMoving", true);

            rigidbody.AddForce(Vector3.forward * (leftThumbStick.y * _speed));
            rigidbody.AddForce(Vector3.right * (leftThumbStick.x * _speed));
        }

        private void Rotate()
        {
            transform.LookAt(transform.position + rigidbody.velocity);

            transform.eulerAngles = new Vector3(
                0,
                transform.eulerAngles.y,
                0);
        }

        #endregion
    }
}