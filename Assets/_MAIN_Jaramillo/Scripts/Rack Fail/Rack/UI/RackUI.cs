﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RackFail
{
    public class RackUI : MonoDebug
    {
        #region Properties

        [Header("Settings"), SerializeField] private Rack rack;

        [Header("UI Settings"), SerializeField]
        private Transform rackUIHolder;

        [Space, SerializeField] private Slider globalValueSlider;
        [SerializeField] private Dictionary<Stats, Slider> statsValuesSliders;

        [Space, SerializeField] private TextMeshProUGUI globalValueTittle;
        [SerializeField] private Dictionary<Stats, TextMeshProUGUI> statsValuesTittle;

        [Space, SerializeField] private TextMeshProUGUI globalValuePercetage;
        [SerializeField] private Dictionary<Stats, TextMeshProUGUI> statsValuesPercentage;

        #endregion

        #region Unity functions

        private void Update()
        {
            UpdateValues();
        }

        #endregion

        #region Class functions

        public override void GetComponents()
        {
            if (rackUIHolder == null)
                rackUIHolder = transform;
        }

        // ***

        public void SetAlpha(float alpha)
        {
            // Setting texts...
            var texts = rackUIHolder.GetComponentsInChildren<TextMeshProUGUI>().Distinct();

            foreach (var text in texts)
                text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);

            // Setting images...
            var images = rackUIHolder.GetComponentsInChildren<Image>().Distinct();

            foreach (var image in images)
                image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
        }

        private void UpdateValues()
        {
            if (rack != null)
            {
                globalValueSlider.value = rack.GlobalValue;

                foreach (var stat in EnumExtentions.GetValues<Stats>().Distinct())
                    statsValuesSliders[stat].value = rack.StatsValues[stat];

                globalValuePercetage.text = string.Format("{0}%", (int) rack.GlobalValue);

                foreach (var stat in EnumExtentions.GetValues<Stats>().Distinct())
                    statsValuesPercentage[stat].text = string.Format("{0}%", (int) statsValuesSliders[stat].value);
            }
        }

        #endregion
    }
}
