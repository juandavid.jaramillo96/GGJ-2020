﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace RackFail
{
    public class Rack : MonoDebug
    {
        #region Properties

        [Header("Settings"), SerializeField] private float decreaseRatio;

        [Header("Collections"), SerializeField] private Dictionary<Stats, float> decreaseValues;

        [SerializeField] private Dictionary<Stats, float> statsValues;

        [Header("Events"), SerializeField] public Dictionary<Stats, DynamicFloatEvent> onStatUpdate;
        [SerializeField] public Dictionary<Stats, DynamicFloatEvent> onStatIncrease;
        [SerializeField] public Dictionary<Stats, DynamicFloatEvent> onStatDecrease;

        // Coroutines
        private IEnumerator decreaseStats;

        // Corutines returns
        private WaitForSeconds waitForSeconds;

        // Getters & Setters
        public float GlobalValue
        {
            get { return statsValues.Average(statsValue => statsValue.Value); }
        }

        public Dictionary<Stats, float> StatsValues => statsValues;

        public float MemoryValue => statsValues[Stats.Memory];
        public float ConectivityValue => statsValues[Stats.Conectivity];
        public float SoftwareValue => statsValues[Stats.Software];
        public float EnergyValue => statsValues[Stats.Energy];

        #endregion

        #region Unity functions

        // ...

        #endregion

        #region Class functions

        public override void Setup()
        {
            StartDecreaseStats();
        }

        // ***

        public void IncreaseMemory(float amount)
        {
            IncreaseStat(Stats.Memory, amount);
        }

        public void DecreaseMemory(float amount)
        {
            DecreaseStat(Stats.Memory, amount);
        }

        public void IncreaseConectivity(float amount)
        {
            IncreaseStat(Stats.Conectivity, amount);
        }

        public void DecreaseConectivity(float amount)
        {
            DecreaseStat(Stats.Conectivity, amount);
        }

        public void IncreaseSoftware(float amount)
        {
            IncreaseStat(Stats.Software, amount);
        }

        public void DecreaseSoftware(float amount)
        {
            DecreaseStat(Stats.Software, amount);
        }

        public void IncreaseEnergy(float amount)
        {
            IncreaseStat(Stats.Energy, amount);
        }

        public void DecreaseEnergy(float amount)
        {
            DecreaseStat(Stats.Energy, amount);
        }

        public void IncreaseStat(Stats stat, float amount)
        {
            statsValues[stat] += Math.Abs(amount);

            statsValues[stat] = Mathf.Clamp(statsValues[stat], 0, 100);

            onStatIncrease[stat]?.Invoke(statsValues[stat]);
        }

        public void DecreaseStat(Stats stat, float amount)
        {
            statsValues[stat] -= Math.Abs(amount);

            statsValues[stat] = Mathf.Clamp(statsValues[stat], 0, 100);

            onStatDecrease[stat]?.Invoke(statsValues[stat]);
        }

        public void IncreaseStats(float amount)
        {
            foreach (var stat in EnumExtentions.GetValues<Stats>().Distinct())
                IncreaseStat(stat, amount);
        }

        public void DecreaseStats(float amount)
        {
            foreach (var stat in EnumExtentions.GetValues<Stats>().Distinct())
                DecreaseStat(stat, amount);
        }

        // ***

        public void StartDecreaseStats()
        {
            StopDecreaseStats();

            decreaseStats = DecreaseStat();

            StartCoroutine(decreaseStats);
        }

        public void StopDecreaseStats()
        {
            if (decreaseStats != null)
            {
                StopCoroutine(decreaseStats);

                decreaseStats = null;
            }
        }

        // ***

        [Button(ButtonSizes.Medium)]
        private void SetDefaultValues()
        {
            if (decreaseValues != null)
                decreaseValues?.Clear();
            else
                decreaseValues = new Dictionary<Stats, float>();

            foreach (var stat in EnumExtentions.GetValues<Stats>())
                decreaseValues.Add(stat, 1);

            // ***

            if (statsValues != null)
                statsValues.Clear();
            else
                statsValues = new Dictionary<Stats, float>();

            foreach (var stat in EnumExtentions.GetValues<Stats>())
                statsValues.Add(stat, 100);
        }

        #endregion

        #region Coroutines

        private IEnumerator DecreaseStat()
        {
            waitForSeconds = new WaitForSeconds(decreaseRatio);

            yield return waitForSeconds;

            do
            {
                foreach (var stat in EnumExtentions.GetValues<Stats>().Distinct())
                {
                    DecreaseStat(stat, decreaseValues[stat]);

                    onStatUpdate[stat]?.Invoke(statsValues[stat]);
                }

                yield return waitForSeconds;

            } while (isActiveAndEnabled);
        }

        #endregion
    }
}
