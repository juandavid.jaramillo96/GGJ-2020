﻿using UnityEngine;

namespace RackFail
{
    public class Glitch : MonoDebug
    {
        #region Properties

        [Header("Glitch Settings"), SerializeField, Range(0, 100)]
        protected float threshold;

        [Header("Dependencies"), SerializeField]
        protected GlitchCameraShader glitchCameraShader;

        // Cached Components
        protected float normalizedValue, normalizedThresholdValue;

        #endregion

        #region Unity functions

        // ...

        #endregion

        #region Class functions

        public override void GetComponents()
        {
            if (glitchCameraShader == null)
                glitchCameraShader = FindObjectOfType<GlitchCameraShader>();
        }

        public virtual void UpdateGlitch(float value)
        {
            if (value >= threshold)
            {
                glitchCameraShader.enabled = false; return;
            }

            normalizedValue = value / 100f;
            normalizedThresholdValue = value / threshold;

            glitchCameraShader.enabled = true;
        }

        #endregion
    }
}
