﻿using UnityEngine;

namespace RackFail
{
    public class MemoryGlitch : Glitch
    {
        #region Properties

        [Header("Memory Settings"), SerializeField, Range(0, 5)]
        private float minGlitchInterval;
        [SerializeField, Range(0, 5)]
        private float maxGlitchInterval;
        [SerializeField, Range(0, 1)]
        private float minGlitchRate, maxGlitchRate;
        [SerializeField, Range(0, 1)]
        private float minWhiteNoiseIntensity, maxWhiteNoiseIntensity;
        [SerializeField, Range(0, 1)]
        private float minWaveNoiseIntensity, maxWaveNoiseIntensity;

        // Cached Components
        private float cachedGlitchInterval, cachedGlitchRate, cachedWhiteNoiseIntensity, cachedWaveNoiseIntensity;

        #endregion

        #region Unity functions

        // ...

        #endregion

        #region Class functions

        public override void UpdateGlitch(float value)
        {
            base.UpdateGlitch(value);

            if (value >= threshold)
            {
                glitchCameraShader.enabled = false; return;
            }

            cachedGlitchInterval = minGlitchInterval + (normalizedThresholdValue * (maxGlitchInterval - minGlitchInterval));
            cachedGlitchRate = minGlitchRate + (normalizedThresholdValue * (maxGlitchRate - minGlitchRate));
            cachedWhiteNoiseIntensity = minWhiteNoiseIntensity + ((1 - normalizedThresholdValue) * (maxWhiteNoiseIntensity - minWhiteNoiseIntensity));
            cachedWaveNoiseIntensity = minWaveNoiseIntensity + ((1 - normalizedThresholdValue) * (maxWaveNoiseIntensity - minWaveNoiseIntensity));

            if (glitchCameraShader != null)
            {
                glitchCameraShader.GlitchInterval = cachedGlitchInterval;
                glitchCameraShader.GlitchRate = cachedGlitchRate;
                glitchCameraShader.WhiteNoiseIntensity = cachedWhiteNoiseIntensity;
                glitchCameraShader.WaveNoiseIntensity = cachedWaveNoiseIntensity;
            }
        }

        #endregion
    }
}
