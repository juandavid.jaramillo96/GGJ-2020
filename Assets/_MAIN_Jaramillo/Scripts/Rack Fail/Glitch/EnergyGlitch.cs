﻿using RackFail;
using UnityEngine;

public class EnergyGlitch : Glitch
{
    #region Properties

    [Header("Energy Settings"), SerializeField, Range(0, 1)]
    private float minAlpha;

    // Cached Components
    private float cachedAlpha;

    #endregion

    #region Unity functions

    // ...

    #endregion

    #region Class functions

    public override void UpdateGlitch(float value)
    {
        base.UpdateGlitch(value);

        if (value >= threshold)
        {
            glitchCameraShader.enabled = false; return;
        }

        cachedAlpha = minAlpha + ((1 - minAlpha) * normalizedThresholdValue);

        if (glitchCameraShader != null)
            glitchCameraShader.GlitchTint = new Color(cachedAlpha, cachedAlpha, cachedAlpha, 1);
    }

    #endregion
}
