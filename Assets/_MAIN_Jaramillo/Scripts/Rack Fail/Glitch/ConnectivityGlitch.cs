﻿using UnityEngine;

namespace RackFail
{
    public class ConnectivityGlitch : Glitch
    {
        #region Properties

        [Header("Software Settings"), SerializeField, Range(0, 1)]
        private float invertHorizontalAxisThreshold;
        [SerializeField, Range(0, 1)]
        private float invertVerticalAxisThreshold;
        [SerializeField, Range(0, 1)]
        private float invertAxisCoordinatesThreshold;

        // Cached Components
        private InputManager[] inputManagers;

        #endregion

        #region Unity functions

        // ...

        #endregion

        #region Class functions

        public override void GetComponents()
        {
            base.GetComponents();

            inputManagers = FindObjectsOfType<InputManager>();
        }

        public override void UpdateGlitch(float value)
        {
            base.UpdateGlitch(value);

            if (value >= threshold)
            {
                glitchCameraShader.enabled = false; return;
            }

            foreach (var inputManager in inputManagers)
            {
                inputManager.invertLeftThumbStickX = normalizedValue <= invertHorizontalAxisThreshold;
                inputManager.invertLeftThumbStickY = normalizedValue <= invertVerticalAxisThreshold;
                inputManager.invertLeftThumbStickAxisCoordinates = normalizedValue <= invertAxisCoordinatesThreshold;
            }
        }

        #endregion
    }
}
