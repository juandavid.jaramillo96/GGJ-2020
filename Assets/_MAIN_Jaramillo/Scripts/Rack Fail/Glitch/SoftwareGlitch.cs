﻿using UnityEngine;

namespace RackFail
{
    public class SoftwareGlitch : Glitch
    {
        #region Properties

        [Header("Software Settings"), SerializeField, Range(0, 1)]
        private float scalineThreshold;
        [SerializeField, Range(0, 1)]
        private float blockThreshold;
        [SerializeField, Range(0, 1)]
        private float shakeThreshold;

        #endregion

        #region Unity functions

        // ...

        #endregion

        #region Class functions

        public override void UpdateGlitch(float value)
        {
            base.UpdateGlitch(value);

            if (value >= threshold)
            {
                glitchCameraShader.enabled = false; return;
            }

            glitchCameraShader.EnableScanlineGlitch = (normalizedValue < scalineThreshold);
            glitchCameraShader.EnableBlockGlitch = (normalizedValue < blockThreshold);
            glitchCameraShader.EnableShakeGlitch = (normalizedValue < shakeThreshold);
        }

        #endregion
    }
}
