﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

namespace RackFail
{
    public class GameManager : MonoDebug
    {
        #region Properties

        [Header("Settings"), SerializeField]
        private Rack rack;

        [SerializeField]
        private GameObject youWon;
        [SerializeField]
        private GameObject youLost;

        [Header("Spawn Settings"), SerializeField]
        private PowerUpSpawner powerUpSpawner;

        [SerializeField]
        private Dictionary<PowerUpTypes, int> spawnCount = new Dictionary<PowerUpTypes, int>();


        [Header("Time"), SerializeField]
        private TimerCountdown timerCountdown;

        // Coroutines
        private Dictionary<Stats, IEnumerator> spawnPerksCoroutines;

        // Corutines returns
        private Dictionary<Stats, WaitForSeconds> waitForSeconds;

        // Singleton!
        public static GameManager Instance
        {
            get; private set;
        }

        #endregion

        #region Unity functions

        protected override void Awake()
        {
            if (Instance != null)
            {
                DestroyImmediate(this); return;
            }

            Instance = this;

            base.Awake();
        }

        private void Update()
        {
            CheckWinCondition();
        }

        #endregion

        #region Class functions

        public override void GetComponents()
        {
            if (timerCountdown == null)
                timerCountdown = FindObjectOfType<TimerCountdown>();
        }

        public override void Suscribe()
        {
            // ...
        }

        public override void Setup()
        {
            if (timerCountdown != null)
                timerCountdown.StartTimer();

            foreach (var powerUpType in EnumExtentions.GetValues<PowerUpTypes>())
            {
                for (var i = 0; i < spawnCount[powerUpType]; i++)
                {
                    powerUpSpawner.SpawnPowerUp(powerUpType);
                }
            }
        }

        // ***

        public void CheckWinCondition()
        {
            if (rack.GlobalValue <= 0)
                YouLost();
        }

        public void CheckWinConditionForTime()
        {
            if (rack.GlobalValue <= 25)
                YouLost();
            else
                YouWon();
        }
        
        // ***

        public void YouWon()
        {
            youWon.SetActive(true);
        }

        public void YouLost()
        {
            youLost.SetActive(true);
        }

        // ***

        [Button(ButtonSizes.Medium)]
        private void SetDefaultValues()
        {
            spawnCount?.Clear();

            foreach (var powerUpTypes in EnumExtentions.GetValues<PowerUpTypes>())
                spawnCount.Add(powerUpTypes, 4);
        }


        #endregion

    }
}

