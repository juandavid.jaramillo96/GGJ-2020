﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuControl : MonoBehaviour
{
    #region Serialized Properties

    #endregion

    #region Properties

    #endregion

    #region Unity Methods

    private void Awake()
    {
        GetComponents();
    }

    private void Start()
    {
        Setup();
    }

    #endregion

    #region Private Methods

    protected virtual void GetComponents()
    {
        //...
    }

    protected virtual void Setup()
    {
        //...
    }


    #endregion

    #region Public Methods

    public void ChangeScene(int sceneIndex)
    {
        SceneLoader.Instance.LoadScene(sceneIndex);
    }

    public void ExitApp()
    {
        Application.Quit();
    }

    #endregion
}
