﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.Events;

public class GameControl : MonoBehaviour
{
    #region Serialized Properties

    [Space, SerializeField]
    private TimeControl timer;

    #endregion

    #region Properties

    #endregion

    #region Unity Methods

    private void Awake()
    {
        GetComponents();
    }

    private void Start()
    {
        Setup();
    }
    
    #endregion

    #region Private Methods

    protected virtual void GetComponents()
    {
        //...
    }

    protected virtual void Setup()
    {
        //...
    }
    
    #endregion

    #region Public Methods

    #endregion
}
