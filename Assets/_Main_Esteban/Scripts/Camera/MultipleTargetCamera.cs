﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace RackFail
{
    [RequireComponent(typeof(Camera))]
    public class MultipleTargetCamera : MonoBehaviour
    {
        #region Serialized Properties

        [Space, SerializeField]
        private Vector3 offset;

        [Space, SerializeField]
        private float smoothTime = 0.5f;

        [Space, SerializeField, Range(10f, 60f)]
        private float minZoom = 50f, maxZoom = 30f;

        [Space, SerializeField]
        private float fieldOfViewLimiter = 60;

        #endregion

        #region Properties

        private List<CharacterController> targets;

        private Vector3 centerVisionPoint;
        private Vector3 newPosiiton;
        private Vector3 velocity;

        private Camera m_camera;

        #endregion

        #region Unity Methods

        private void Awake()
        {
            GetComponents();
        }

        private void Start()
        {
            Setup();
        }

        private void LateUpdate()
        {
            MovingCamera();
            ZoomingCamera();
        }

        #endregion

        #region Private Methods

        private void ZoomingCamera()
        {
            float newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreaterDistance() / fieldOfViewLimiter);
            m_camera.fieldOfView = Mathf.Lerp(m_camera.fieldOfView, newZoom, Time.deltaTime);
        }

        private float GetGreaterDistance()
        {
            var bounds = new Bounds(targets[0].transform.position, Vector3.zero);

            for (int i = 0; i < targets.Count; i++)
            {
                bounds.Encapsulate(targets[i].transform.position);
            }

            return bounds.size.x;
        }

        private void MovingCamera()
        {
            if (targets.Count == 0)
                return;

            centerVisionPoint = GetCenterPoint();

            newPosiiton = centerVisionPoint + offset;

            transform.position = Vector3.SmoothDamp(transform.position, newPosiiton, ref velocity, smoothTime);
        }

        private Vector3 GetCenterPoint()
        {
            if (targets.Count == 1)
            {
                return targets[0].transform.position;
            }

            var bounds = new Bounds(targets[0].transform.position, Vector3.zero);

            for (int i = 0; i < targets.Count; i++)
            {
                bounds.Encapsulate(targets[i].transform.position);
            }

            return bounds.center;

        }

        protected virtual void GetComponents()
        {
            targets = FindObjectsOfType<CharacterController>().ToList<CharacterController>();

            m_camera = GetComponent<Camera>();
        }

        protected virtual void Setup()
        {
            //...
        }


        #endregion

        #region Public Methods

        #endregion
    }
}
