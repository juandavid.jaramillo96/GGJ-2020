﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RackFail
{
    public class ObjectPicker : MonoBehaviour
    {
        private Inventory m_inventory;
        private InputManager m_input;

        private CharacterController m_characterController; //Esta lógica deberia hacerce en otra script pero me da flojera
        private Animator m_animator;
        private Rigidbody m_rigidbody;

        private float initialSpeed, initialMaxSpeed;

        private void Awake()
        {
            GetComponents();
        }

        private void Start()
        {
            Setup();
        }

        private void Setup()
        {
            initialSpeed = m_characterController.speed;
            initialMaxSpeed = m_characterController.maxSpeed;
        }

        private void GetComponents()
        {
            m_inventory = GetComponent<Inventory>();
            m_input = GetComponent<InputManager>();

            m_characterController = GetComponent<CharacterController>();
            m_animator = GetComponent<Animator>();
            m_rigidbody = GetComponent<Rigidbody>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "PowerUp" && m_inventory.GetCurrentInventoryCapacity() > 0)
            {
                CollectTheObject(other.name);
                Destroy(other.gameObject);
            }
        }

        private void CollectTheObject(string objectName)
        {
            switch (objectName)
            {
                case "Memory":
                    m_inventory.AddMemoryPower();
                    break;
                case "Connectivity":
                    m_inventory.AddConnectivityPower();
                    break;
                case "Software":
                    m_inventory.AddSoftwarePower();
                    break;
                case "Energy":
                    m_inventory.AddEnergyPower();
                    break;
                case "Memory(Clone)":
                    m_inventory.AddMemoryPower();
                    break;
                case "Connectivity(Clone)":
                    m_inventory.AddConnectivityPower();
                    break;
                case "Software(Clone)":
                    m_inventory.AddSoftwarePower();
                    break;
                case "Energy(Clone)":
                    m_inventory.AddEnergyPower();
                    break;
                default:
                    break;
            }
        }

        private void Update()
        {
            if (m_input.GetButtonDown(ButtonIdentifier.X) && m_inventory.GetCurrentInventoryCapacity() < 3 && m_rigidbody.velocity.magnitude <= 0.5f)
            {
                //m_animator.SetTrigger("Release");
                m_inventory.DestroyTheLastCollectedObject();
            }

            CheckInventoryState();
        }

        private void CheckInventoryState()
        {
            int capacity = m_inventory.GetCurrentInventoryCapacity();
            if (capacity == 3)
            {
                m_characterController.SetSpeed(initialSpeed);
                m_characterController.SetMaxSpeed(initialMaxSpeed);
                m_animator.speed = 1;
                return;
            }
            if (capacity == 2)
            {
                m_characterController.SetSpeed(initialSpeed * 0.85f);
                m_characterController.SetMaxSpeed(initialMaxSpeed * 0.85f);
                m_animator.speed = 0.85f;
                return;
            }
            if (capacity == 1)
            {
                m_characterController.SetSpeed(initialSpeed * 0.70f);
                m_characterController.SetMaxSpeed(initialMaxSpeed * 0.70f);
                m_animator.speed = 0.7f;
                return;
            }
            if (capacity == 0)
            {
                m_characterController.SetSpeed(initialSpeed * 0.5f);
                m_characterController.SetMaxSpeed(initialMaxSpeed * 0.5f);
                m_animator.speed = 0.5f;
                return;
            }
        }

    }
}

