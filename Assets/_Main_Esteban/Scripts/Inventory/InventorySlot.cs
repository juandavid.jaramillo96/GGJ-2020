﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RackFail
{
    public class InventorySlot : MonoDebug
    {
        private Dictionary<PowerUpTypes, GameObject> powerUpsImages = new Dictionary<PowerUpTypes, GameObject>();

        //Cached

        public GameObject currentPowerUp
        {
            get; private set;
        }

        private void Start()
        {
            GetImages();
            HideAll();
        }

        private void GetImages()
        {
            foreach (Transform child in transform)
            {
                if (child.GetComponent<Image>())
                {
                    switch (child.name)
                    {
                        case "Memory":
                            powerUpsImages.Add(PowerUpTypes.Memory, child.gameObject);
                            break;
                        case "Connectivity":
                            powerUpsImages.Add(PowerUpTypes.Connectivity, child.gameObject);
                            break;
                        case "Software":
                            powerUpsImages.Add(PowerUpTypes.Software, child.gameObject);
                            break;
                        case "Energy":
                            powerUpsImages.Add(PowerUpTypes.Energy, child.gameObject);
                            break;

                        default:
                            break;
                    }
                    
                }
            }
        }

        public void HideAll()
        {
            currentPowerUp = null;
            foreach (var powerUp in powerUpsImages)
            {
                powerUp.Value.SetActive(false);
            }
        }

        public Dictionary<PowerUpTypes, GameObject> GetPowerUps()
        {
            return powerUpsImages;
        }

        public void SetPowerUp(PowerUpTypes powerUpType)
        {
            HideAll();
            currentPowerUp = powerUpsImages[powerUpType];
            powerUpsImages[powerUpType].SetActive(true);
        }
    }
}

