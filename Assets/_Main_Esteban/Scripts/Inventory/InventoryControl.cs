﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RackFail
{
    public class InventoryControl : MonoBehaviour
    {
        [Space, SerializeField]
        private InventorySlot slot01, slot02, slot03;

        private void Start()
        {
            Setup();
        }

        private void Setup()
        {
            slot01.HideAll();
            slot02.HideAll();
            slot03.HideAll();
        }

        public void PickUpPower(PowerUpTypes powerUpType)
        {
            if (slot01.currentPowerUp == null)
            {
                slot01.SetPowerUp(powerUpType);
                return;
            }

            if (slot02.currentPowerUp == null)
            {
                slot02.SetPowerUp(powerUpType);
                return;
            }

            if (slot03.currentPowerUp == null)
            {
                slot03.SetPowerUp(powerUpType);
                return;
            }
        }

        public void DestroyObject(int slotIndex)
        {
            switch (slotIndex)
            {
                case 1:
                    slot01.HideAll();
                    break;
                case 2:
                    slot02.HideAll();
                    break;
                case 3:
                    slot03.HideAll();
                    break;
                default:
                    break;
            }
        }

        public void DestoyTheLastItemCollected()
        {
            if (slot03.currentPowerUp != null)
            {
                DestroyObject(3);
                return;
            }

            if (slot02.currentPowerUp != null)
            {
                DestroyObject(2);
                return;
            }

            if (slot01.currentPowerUp != null)
            {
                DestroyObject(1);
                return;
            }
        }

        private PowerUpTypes? KnowTheType(string name)
        {
            switch (name)
            {
                case "Memory":
                    return PowerUpTypes.Memory;
                    
                case "Connectivity":
                    return PowerUpTypes.Connectivity;

                case "Software":
                    return PowerUpTypes.Software;

                case "Energy":
                    return PowerUpTypes.Energy;

                default:
                    return null;
            }
        }

        public PowerUpTypes? GetTheLasItemCollected()
        {
            if (slot03.currentPowerUp != null)
            {
                foreach (var powerUp in slot03.GetPowerUps())
                {
                    if (powerUp.Value.activeSelf)
                    {
                        if (KnowTheType(powerUp.Value.name) != null)
                            return KnowTheType(powerUp.Value.name);
                    }
                }
            }

            if (slot02.currentPowerUp != null)
            {
                foreach (var powerUp in slot02.GetPowerUps())
                {
                    if (powerUp.Value.activeSelf)
                    {
                        if (KnowTheType(powerUp.Value.name) != null)
                            return KnowTheType(powerUp.Value.name);
                    }
                }
            }

            if (slot01.currentPowerUp != null)
            {
                foreach (var powerUp in slot01.GetPowerUps())
                {
                    if (powerUp.Value.activeSelf)
                    {
                        if (KnowTheType(powerUp.Value.name) != null)
                            return KnowTheType(powerUp.Value.name);
                    }
                }
            }
            print("Can not find");
            return null;
        }
        
        public int GetCurrentInventoryCapacity()
        {
            int capacity = 3;

            if (slot01.currentPowerUp != null)
                capacity--;
            if (slot02.currentPowerUp != null)
                capacity--;
            if (slot03.currentPowerUp != null)
                capacity--;

            return capacity;
        }
    }
}
