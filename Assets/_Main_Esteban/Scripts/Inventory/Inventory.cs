﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RackFail
{
    public class Inventory : MonoBehaviour
    {
        [Space, SerializeField]
        private InventoryControl m_control;

        public void AddMemoryPower()
        {
            m_control.PickUpPower(PowerUpTypes.Memory);
        }

        public void AddConnectivityPower()
        {
            m_control.PickUpPower(PowerUpTypes.Connectivity);
        }

        public void AddSoftwarePower()
        {
            m_control.PickUpPower(PowerUpTypes.Software);
        }

        public void AddEnergyPower()
        {
            m_control.PickUpPower(PowerUpTypes.Energy);
        }

        public void DestroyTheLastCollectedObject()
        {
            m_control.DestoyTheLastItemCollected();
        }

        public int GetCurrentInventoryCapacity()
        {
            return m_control.GetCurrentInventoryCapacity();
        }

        public PowerUpTypes GetTheLastTipePowerCollected()
        {
            return m_control.GetTheLasItemCollected().Value;
        }
    }
}
