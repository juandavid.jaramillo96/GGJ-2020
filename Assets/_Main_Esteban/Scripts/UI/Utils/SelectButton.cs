﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectButton : MonoBehaviour
{
    [SerializeField]
    private Button firstButton;

    private void Start()
    {
        SelectFirstButton();
    }

    private void OnEnable()
    {
        SelectFirstButton();
    }

    public void SelectFirstButton()
    {
        if (firstButton == null)
            FindObjectOfType<Button>().Select();
        else
            firstButton.Select();
    }
}
