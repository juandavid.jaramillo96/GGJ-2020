﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RackFail
{
    public class UIHolder : Sirenix.OdinInspector.SerializedMonoBehaviour
    {

        #region Serialized Properties

        [SerializeField, Space]
        private string initialScreen;

        [SerializeField, Space]
        private Dictionary<string, GameObject> ui_screens;

        #endregion

        #region Properties
        
        #endregion

        #region Unity Methods

        private void Awake()
        {
            GetComponents();
        }

        private void Start()
        {
            Setup();
        }

        #endregion

        #region Private Methods

        protected virtual void GetComponents()
        {
            //...
        }

        protected virtual void Setup()
        {
            if (initialScreen != null)
                ShowScreen(initialScreen);
            else
                Debug.LogError("Set some initial screen");
        }

        private void HideAllScreens()
        {
            foreach (GameObject ui in ui_screens.Values)
            {
                ui.SetActive(false);
            }
        }

        #endregion

        #region Public Methods

        public void ShowScreen(string screenName)
        {
            HideAllScreens();
            if (ui_screens.ContainsKey(screenName))
                ui_screens[screenName].SetActive(true);
            else
                Debug.LogError("Incorrect screen name");
        }

        #endregion

    }
}


