﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.AI;

namespace RackFail
{
    public class PowerUpSpawner : Sirenix.OdinInspector.SerializedMonoBehaviour
    {
        #region Serialized Properties

        [Space, SerializeField, FoldoutGroup("Power Up pool collection")]
        private Dictionary<PowerUpTypes, PoolClass> PowerUpLibrary;

        [Space, SerializeField, FoldoutGroup("Spawner settings"), Range(0, 50)]
        private float maxRadiusDistance = 16, minRadiusDistance = 4;

        #endregion

        #region Properties

        #endregion

        #region Unity Methods

        private void Awake()
        {
            GetComponents();
        }

        private void Start()
        {
            Setup();
        }

        public void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, maxRadiusDistance);

            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, minRadiusDistance);
        }

        #endregion

        #region Private Methods

        protected virtual void GetComponents()
        {
            //...
        }

        protected virtual void Setup()
        {
            //...
        }

        private Vector3 GetPointDestination()
        {
            Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * maxRadiusDistance;
            randomDirection += transform.position;
            NavMeshHit hit;
            Vector3 finalPosition = Vector3.zero;
            if (NavMesh.SamplePosition(randomDirection, out hit, maxRadiusDistance, 1))
            {
                finalPosition = hit.position;
            }
            return finalPosition  + Vector3.up;
        }

        #endregion

        #region Public Methods

        public void SpawnPowerUp(int powerUpTypeIndex)
        {
            PowerUpTypes powerUpType = (PowerUpTypes)powerUpTypeIndex;

            SpawnPowerUp(powerUpType);
        }

        public void SpawnPowerUp(PowerUpTypes powerUpType)
        {
            Vector3 newDestination = GetPointDestination();

            if (Vector3.Distance(transform.position, newDestination) < minRadiusDistance || newDestination == (Vector3.zero + Vector3.up))
            {
                SpawnPowerUp(powerUpType);
                //Debug.Log("Obteniendo un nuevo destino...");
                return;
            }

            PowerUpLibrary[powerUpType].GetObject(newDestination);
        }

        #endregion
    }
}
