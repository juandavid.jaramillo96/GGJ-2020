﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RackFail
{
    public class RackFixer : MonoDebug
    {
        #region Serialized Properties

        [Space, SerializeField]
        private Rack rack;

        [Space, SerializeField]
        private Canvas canvas;

        [Space, SerializeField]
        private Slider slider;

        [Space, SerializeField]
        private float sliderSpeed, time;

        [Space, SerializeField]
        private List<LeanTweenType> leanTweenCurves;

        [Space, SerializeField]
        private Image powerUpSlot;

        [Space, SerializeField]
        private Dictionary<PowerUpTypes, Sprite> powerUpSprites;

        [Space, SerializeField]
        private Dictionary<Stats, int> repairCapacityPerStack;

        [Space, SerializeField] 
        private float cooldownTime = 1.5f;

        #endregion

        #region Properties

        private Inventory c_inventory;
        private InputManager c_inputManager;

        private bool inForntOfRack = false;

        private LTDescr lean;

        private bool tweenDirection = true;
        private bool isOnCooldown = false;

        private GameObject currentCharacter = null;
        private PowerUpTypes? currentPowerType;

        #endregion

        #region Unity Methods

        private void Awake()
        {
            //GetComponents();
        }

        private void Start()
        {
            Setup();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Character" && currentCharacter == null)
            {

                currentCharacter = other.gameObject;
                GetComponents();

                if(c_inventory.GetCurrentInventoryCapacity() < 3)
                {
                    inForntOfRack = true;
                    canvas.gameObject.SetActive(true);

                    SetSpriteOnImage();

                    StartCoroutine("FollowInputs");

                    lean = LeanTween
                        .value(gameObject: gameObject, from: 0, to: 1, time: time, callOnUpdate: OnTweenUpdate)
                        .setEase(leanTweenCurves[Random.Range(0, leanTweenCurves.Count - 1)]);
                    lean.setOnComplete(OnTweenComplete);
                }
                else
                {
                    currentCharacter = null;
                }
            }
            else
            {
                print("Rack is using by: " + currentCharacter.name);
            }
        }

        private void SetSpriteOnImage()
        {
            currentPowerType = c_inventory.GetTheLastTipePowerCollected();
            powerUpSlot.sprite = powerUpSprites[currentPowerType.Value];
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Character" && other.gameObject == currentCharacter)
            {
                StopFixing();
            }
        }

        #endregion

        #region Private Methods

        protected virtual void GetComponents()
        {
            c_inventory = currentCharacter.GetComponent<Inventory>();
            c_inputManager = currentCharacter.GetComponent<InputManager>();
        }

        protected virtual void Setup()
        {
            canvas.gameObject.SetActive(false);
        }

        private void OnTweenUpdate(float value)
        {
            slider.value = value;
        }

        private void OnTweenUpdateNegative(float value)
        {
            slider.value = value;
        }

        private void OnTweenComplete()
        {
            if (tweenDirection)
            {
                LeanTween
                    .value(gameObject: gameObject, from: 1, to: 0, time: time, callOnUpdate: OnTweenUpdateNegative)
                    .setEase(leanTweenCurves[Random.Range(0, leanTweenCurves.Count - 1)]).setOnComplete(OnTweenComplete);
            }
            else
            {
                LeanTween
                    .value(gameObject: gameObject, from: 0, to: 1, time: time, callOnUpdate: OnTweenUpdate)
                    .setEase(leanTweenCurves[Random.Range(0, leanTweenCurves.Count - 1)]).setOnComplete(OnTweenComplete);
            }
            tweenDirection = !tweenDirection;
        }

        private IEnumerator FollowInputs()
        {
            while (inForntOfRack)
            {
                if (c_inputManager.GetButtonDown(ButtonIdentifier.A) && !isOnCooldown)
                {
                    UseThePower();
                    isOnCooldown = true;
                    StartCoroutine(CoolDownWait());
                }
                if (c_inputManager.GetButtonDown(ButtonIdentifier.X))
                {
                    StopFixing();
                }
                yield return null;
            }

        }

        private IEnumerator CoolDownWait()
        {
            yield return new WaitForSeconds(cooldownTime);

            isOnCooldown = false;

            StopCoroutine(CoolDownWait());

        }

        private void UseThePower()
        {
            LeanTween.cancel(this.gameObject);
            c_inventory.DestroyTheLastCollectedObject();
            CalculatePoints();
            Invoke("StopFixing", 1f);
        }

        private void CalculatePoints()
        {
            float sliderZone = slider.value;

            switch (currentPowerType.Value)
            {
                case PowerUpTypes.Energy:

                    print(repairCapacityPerStack[Stats.Energy] * sliderZone);
                    rack.IncreaseStat(Stats.Energy, repairCapacityPerStack[Stats.Energy] * sliderZone);

                    break;
                case PowerUpTypes.Memory:

                    print(repairCapacityPerStack[Stats.Memory] * sliderZone);
                    rack.IncreaseStat(Stats.Memory, repairCapacityPerStack[Stats.Memory] * sliderZone);

                    break;
                case PowerUpTypes.Software:

                    print(repairCapacityPerStack[Stats.Software] * sliderZone);
                    rack.IncreaseStat(Stats.Software, repairCapacityPerStack[Stats.Software] * sliderZone);

                    break;
                case PowerUpTypes.Connectivity:

                    print(repairCapacityPerStack[Stats.Conectivity] * sliderZone);
                    rack.IncreaseStat(Stats.Conectivity, repairCapacityPerStack[Stats.Conectivity] * sliderZone);

                    break;
                default:
                    break;
            }

        }

        private void StopFixing()
        {
            inForntOfRack = false;
            canvas.gameObject.SetActive(false);
            StopCoroutine("FollowInputs");
            LeanTween.cancel(this.gameObject);
            currentCharacter = null;
            currentPowerType = null;
        }

        #endregion

        #region Public Methods

        #endregion
    }
}

