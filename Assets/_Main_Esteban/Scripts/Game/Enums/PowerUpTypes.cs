﻿namespace RackFail
{
    public enum PowerUpTypes
    {
        Energy,
        Memory,
        Software,
        Connectivity
    }
}
