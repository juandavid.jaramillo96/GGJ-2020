﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.Events;

public class TimeControl : MonoBehaviour
{
    #region Serialized Properties

    [Space, SerializeField]
    private float time = 100;

    [Space, SerializeField, FoldoutGroup("UI objects")]
    private TextMeshProUGUI txt_time;

    #endregion

    #region Properties

    private Coroutine timeUpdate;

    public UnityEvent OnTimeComplete;

    #endregion

    #region Unity Methods

    private void Start()
    {
        Setup();
    }

    #endregion

    #region Private Methods
    
    protected virtual void Setup()
    {
        timeUpdate = StartCoroutine(TimeUpdate());
    }

    IEnumerator TimeUpdate()
    {
        while (time > 0)
        {
            time -= Time.deltaTime;

            if (txt_time != null)
                txt_time.text = $"Time: {time.ToString("f0")}";

            yield return null;
        }

        OnTimeComplete.Invoke();
    }

    #endregion

    #region Public Methods

    public float GetCurrentTime()
    {
        return time;
    }

    #endregion
}

